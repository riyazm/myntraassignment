package com.riyazm.memorygame.test;

import com.riyazm.memorygame.home.HomePresenter;
import com.riyazm.memorygame.home.HomeView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by muhammadriyaz on 09/03/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {
    @Mock
    HomeView homeView;
    private HomePresenter homePresenter;

    @Before
    public void setUp() throws Exception {
        homePresenter = new HomePresenter(homeView);
    }

    @Test
    public void startGameWhenStartButtonTapped() throws Exception {
        homePresenter.beginGame();
        verify(homeView).startGame();
    }

}