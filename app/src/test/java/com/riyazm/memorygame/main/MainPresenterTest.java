package com.riyazm.memorygame.main;

import com.riyazm.memorygame.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by muhammadriyaz on 09/03/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    MainView mainView;

    MainPresenter mainPresenter;

    @Before
    public void setUp() throws Exception {
        mainPresenter = new MainPresenter(mainView);

    }

    @Test
    public void stopWhenWrongPhotoTapped() throws Exception {
        when(mainView.getSelectedRandomIndex()).thenReturn(0);
        when(mainView.getTappedIndex()).thenReturn(1);

        mainPresenter.revealPhoto();
        verify(mainView).showError(R.string.try_again);
    }

    @Test
    public void goAheadWhenCorrectImageTapped() throws Exception {
        when(mainView.getSelectedRandomIndex()).thenReturn(0);
        when(mainView.getTappedIndex()).thenReturn(0);

        mainPresenter.revealPhoto();
        verify(mainView).revealPhoto(0);
    }


    @Test
    public void gameOverWhenNoMoreToReveal() throws Exception {
        when(mainView.getShuffledStackSize()).thenReturn(0);
        mainPresenter.pickAndShowAPhotoNow();
        verify(mainView).gameOver();

    }

    @Test
    public void continueWhenThereAreMorePhotos() throws Exception {
        when(mainView.getShuffledStackSize()).thenReturn(1);
        mainPresenter.pickAndShowAPhotoNow();
        verify(mainView).pickNextRandomPhoto();

    }
}