package com.riyazm.memorygame.main;

import com.riyazm.memorygame.R;

/**
 * Created by muhammadriyaz on 09/03/17.
 */

public class MainPresenter {
    private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }


    public void revealPhoto() {
        if (view.getSelectedRandomIndex() != view.getTappedIndex()) {
            view.showError(R.string.try_again);
        } else {
            view.revealPhoto(view.getTappedIndex());
        }
    }

    public void pickAndShowAPhotoNow() {
        if (view.getShuffledStackSize() == 0) {
            view.gameOver();
            return;
        }

        view.pickNextRandomPhoto();
    }
}
