package com.riyazm.memorygame.main;

/**
 * Created by muhammadriyaz on 09/03/17.
 */

public interface MainView {

    void showError(int resourceID);
    int getSelectedRandomIndex();
    int getTappedIndex();
    void gameOver();
    int getShuffledStackSize();

    void revealPhoto(int position);

    void pickNextRandomPhoto();
}
