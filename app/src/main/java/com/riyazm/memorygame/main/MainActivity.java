package com.riyazm.memorygame.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.riyazm.memorygame.R;
import com.riyazm.memorygame.adapters.PhotosListAdapter;
import com.riyazm.memorygame.interfaces.ListItemClickInterface;
import com.riyazm.memorygame.models.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity implements MainView {
    private static final String TAG = MainActivity.class.getSimpleName();

    Button startButton;
    RecyclerView photosListRecyclerView;
    PhotosListAdapter photosListAdapter;
    ArrayList<Photo> photos = new ArrayList<>();
    final GridLayoutManager mLayoutManager = new GridLayoutManager(this, 3);
    TextView countDown;
    CountDownTimer timer;
    boolean isTimerRunning = true;
    int selectedRandomIndex;
    int tappedIndex;
    ImageView photoToFind;
    boolean isGameStarted = false;
    Stack<Integer> shuffledStack = new Stack<>();
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenter(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            Log.e(TAG, "no extras passed");
            finish();
            return;
        }
        photos = bundle.getParcelableArrayList("photos");
        if ((photos != null ? photos.size() : 0) == 0) {
            Log.e(TAG, "0 photos");
            finish();
            return;
        }

        // prepare our shuffled stack
        for (int i = 0; i < photos.size(); i++) {
            shuffledStack.push(i);
        }
        Collections.shuffle(shuffledStack);
        Log.d(TAG, "shuffled: " + shuffledStack.toString());

        startButton = (Button) findViewById(R.id.start_btn);
        countDown = (TextView) findViewById(R.id.count_down);
        photoToFind = (ImageView) findViewById(R.id.photo_to_find);
        photosListRecyclerView = (RecyclerView) findViewById(R.id.photos_list);
        photosListRecyclerView.setHasFixedSize(true);
        photosListRecyclerView.setLayoutManager(mLayoutManager);
        photosListAdapter = new PhotosListAdapter(R.layout.photo_list_item, photos, this);
        photosListAdapter.setListItemClickInterface(new ListItemClickInterface() {
            @Override
            public void onClick(int position) {
                tappedIndex = position;
                if (!photos.get(position).reveal) {
                    if (isGameStarted) {
                        mainPresenter.revealPhoto();
                    } else {
                        Toast.makeText(MainActivity.this, "Press start to begin", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.i(TAG, "clicked on already revealed photo");
                }
            }
        });
        photosListRecyclerView.setAdapter(photosListAdapter);
        timer = new CountDownTimer(15000, 1000) {
            public void onTick(long millisUntilFinished) {
                isTimerRunning = true;
                countDown.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                isTimerRunning = false;
                onTimerExpired();
            }
        };
    }


    void revealAllPhotos() {
        for (Photo photo : photos) {
            photo.reveal = true;
        }
        photosListAdapter.notifyDataSetChanged();
    }

    void hideAllPhotos() {
        for (Photo photo : photos) {
            photo.reveal = false;
        }
        photosListAdapter.notifyDataSetChanged();
    }


    void pickAndShowAPhotoNow() {
        if (getShuffledStackSize() == 0) {
            gameOver();
            return;
        }

        selectedRandomIndex = shuffledStack.pop();
        photoToFind.setVisibility(View.VISIBLE);
        Picasso.with(this)
                .load(photos.get(selectedRandomIndex).media.m)
                .into(photoToFind);
    }

    @Override
    public void gameOver() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Game Over");

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        finish();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
        isGameStarted = false;
    }

    @Override
    public int getShuffledStackSize() {
        return shuffledStack.size();
    }


    public void onStartClicked(View view) {
        startButton.setVisibility(GONE);
        countDown.setVisibility(View.VISIBLE);

        // reveal all the pics first
        revealAllPhotos();

        if (isTimerRunning) {
            timer.cancel();
        }
        timer.start();
    }

    void onTimerExpired() {
        countDown.setVisibility(GONE);
        hideAllPhotos();
        mainPresenter.pickAndShowAPhotoNow();
        isGameStarted = true;
    }

    @Override
    public void showError(int resourceID) {
        Toast.makeText(this, resourceID, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getSelectedRandomIndex() {
        return selectedRandomIndex;
    }

    @Override
    public int getTappedIndex() {
        return tappedIndex;
    }

    @Override
    public void revealPhoto(int position) {
        photos.get(position).reveal = true;
        photosListAdapter.notifyItemChanged(position);
        mainPresenter.pickAndShowAPhotoNow();
    }

    @Override
    public void pickNextRandomPhoto() {
        selectedRandomIndex = shuffledStack.pop();
        photoToFind.setVisibility(View.VISIBLE);
        Picasso.with(this)
                .load(photos.get(selectedRandomIndex).media.m)
                .into(photoToFind);
    }
}
