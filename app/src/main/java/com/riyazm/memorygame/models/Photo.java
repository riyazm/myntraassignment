package com.riyazm.memorygame.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by muhammadriyaz on 07/03/17.
 */

public class Photo implements Parcelable {

    /**
     * "title": "Scarborough. Our visit to North Yorkshire. UK. 4-6 Mar'17",
     * "link": "https:\/\/www.flickr.com\/photos\/sigita_manikaite\/32461484374\/",
     * "media": {"m":"https:\/\/farm4.staticflickr.com\/3910\/32461484374_c9fb88d300_m.jpg"},
     * "date_taken": "2017-03-07T13:36:45-08:00",
     * "description": " <p><a href=\"https:\/\/www.flickr.com\/people\/sigita_manikaite\/\">Sigita Manite<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/sigita_manikaite\/32461484374\/\" title=\"Scarborough. Our visit to North Yorkshire. UK. 4-6 Mar'17\"><img src=\"https:\/\/farm4.staticflickr.com\/3910\/32461484374_c9fb88d300_m.jpg\" width=\"180\" height=\"240\" alt=\"Scarborough. Our visit to North Yorkshire. UK. 4-6 Mar'17\" \/><\/a><\/p> ",
     * "published": "2017-03-07T13:36:45Z",
     * "author": "nobody@flickr.com (\"Sigita Manite\")",
     * "author_id": "49585698@N04",
     * "tags": ""
     */

    public String title, link;
    public PhotoMedia media;
    public boolean reveal = false;

    protected Photo(Parcel in) {
        title = in.readString();
        link = in.readString();
        reveal = in.readByte() != 0;
        media = in.readParcelable(PhotoMedia.class.getClassLoader());
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(link);
        parcel.writeByte((byte) (reveal ? 1 : 0));
        parcel.writeParcelable(media, i);
    }


    @Override
    public String toString() {
        return "Photo{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", media=" + media +
                '}';
    }
}
