package com.riyazm.memorygame.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by muhammadriyaz on 08/03/17.
 */

public class PhotoMedia implements Parcelable {

    public String m;

    protected PhotoMedia(Parcel in) {
        m = in.readString();
    }

    public static final Creator<PhotoMedia> CREATOR = new Creator<PhotoMedia>() {
        @Override
        public PhotoMedia createFromParcel(Parcel in) {
            return new PhotoMedia(in);
        }

        @Override
        public PhotoMedia[] newArray(int size) {
            return new PhotoMedia[size];
        }
    };

    @Override
    public String toString() {
        return "PhotoMedia{" +
                "m='" + m + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(m);
    }

}
