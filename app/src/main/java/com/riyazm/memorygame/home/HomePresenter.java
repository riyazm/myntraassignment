package com.riyazm.memorygame.home;

import com.riyazm.memorygame.R;

/**
 * Created by muhammadriyaz on 09/03/17.
 */

public class HomePresenter {

    private HomeView view;

    public HomePresenter(HomeView view) {
        this.view = view;
    }

    public void beginGame() {
        if (view.getFetchedImagesCount() < view.getTotalPhotosToBeFetchedCount()) {
            view.showError(R.string.failed_to_retrieve_assets);
            return;
        }
        view.startGame();
    }
}
