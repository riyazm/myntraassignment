package com.riyazm.memorygame.home;

/**
 * Created by muhammadriyaz on 09/03/17.
 */

public interface HomeView {
    void startGame();

    int getFetchedImagesCount();

    int getTotalPhotosToBeFetchedCount();

    void showError(int string_resource);
}
