package com.riyazm.memorygame.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.riyazm.memorygame.R;
import com.riyazm.memorygame.constants.Endpoints;
import com.riyazm.memorygame.interfaces.APICallbacks;
import com.riyazm.memorygame.main.MainActivity;
import com.riyazm.memorygame.models.Photo;
import com.riyazm.memorygame.utils.APIs;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements HomeView {
    private static final String TAG = HomeActivity.class.getSimpleName();

    Button beginGameBtn;
    ProgressBar progressBar;
    TextView loaderMessage;
    int MAX_PHOTOS = 9;
    ArrayList<Photo> photos = new ArrayList<>();
    HomePresenter presenter;
    int fetchedImagesCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        presenter = new HomePresenter(this);
        beginGameBtn = (Button) findViewById(R.id.begin_game_btn);
        progressBar = (ProgressBar) findViewById(R.id.list_loader);
        loaderMessage = (TextView) findViewById(R.id.loader_message);
        getPictures();
    }


    void setLoaderVisibility(boolean status) {
        progressBar.setVisibility(status ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void startGame() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("photos", photos);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public int getFetchedImagesCount() {
        return fetchedImagesCount;
    }

    @Override
    public int getTotalPhotosToBeFetchedCount() {
        return photos.size();
    }

    public void onclickBeginGameBtn(View view) {
        presenter.beginGame();
    }

    public void getPictures() {
        // Tag used to cancel the request
        String tag_json_obj = "photo_list_request";
        APIs.requestString(Endpoints.PHOTOS_LIST, Request.Method.GET, tag_json_obj, new APICallbacks() {
            @Override
            public void onPreExecute(String requestURL) {
                super.onPreExecute(requestURL);
                setLoaderVisibility(true);
            }

            @Override
            public void onPostExecuteSuccess(String requestURL, String response) {
                super.onPostExecuteSuccess(requestURL, response);
                List<Photo> temp = new ArrayList<>();
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.optJSONArray("items");

                    // we want only first 9
                    for (int i = 0; i < MAX_PHOTOS; i++) {
                        Gson gson = new Gson();
                        final Photo photo = gson.fromJson(array.optJSONObject(i).toString(), Photo.class);
                        temp.add(photo);
                    }
                    Log.d(TAG, "photos: " + temp);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                photos.addAll(temp);
                if (photos.size() > 0)
                    prefetchPhotos(0);
            }

            @Override
            public void onPostExecuteFail(String requestURL, VolleyError error) {
                super.onPostExecuteFail(requestURL, error);
                Log.e(TAG, "error: " + error.getMessage());
                showError(R.string.something_went_wrong);
                // hide the progress dialog
                setLoaderVisibility(false);
            }
        });
    }

    void setLoaderMessage(String message) {
        loaderMessage.setText(message);
    }

    void prepareToBegin() {
        setLoaderVisibility(false);
        beginGameBtn.setVisibility(View.VISIBLE);
        setLoaderMessage("");
    }

    void prefetchPhotos(final int startIndex) {

        Picasso.with(getApplicationContext())
                .load(photos.get(startIndex).media.m)
                .fetch(new Callback() {
                    @Override
                    public void onSuccess() {
                        fetchedImagesCount++;
                        if (startIndex >= photos.size() - 1) {
                            prepareToBegin();
                        } else {
                            Log.d(TAG, "loaded: " + photos.get(startIndex).media.m);
                            setLoaderMessage(getString(R.string.fetching_assets)
                                    .replace("@index", "" + (startIndex + 1))
                                    .replace("@size", "" + photos.size()));

                            prefetchPhotos(startIndex + 1);
                        }
                    }

                    @Override
                    public void onError() {
                        showError(R.string.failed_to_retrieve_assets);
                        Log.e(TAG, "failed to download " + photos.get(startIndex).media.m);
                    }
                });
    }

    @Override
    public void showError(int resourceID) {
        Toast.makeText(this, resourceID, Toast.LENGTH_SHORT).show();
    }
}
