package com.riyazm.memorygame.constants;

/**
 * Created by muhammadriyaz on 07/03/17.
 */

public class Endpoints {
    private static final String SERVER = "https://api.flickr.com/";
    public static final String PHOTOS_LIST = SERVER + "services/feeds/photos_public.gne?format=json";
}
