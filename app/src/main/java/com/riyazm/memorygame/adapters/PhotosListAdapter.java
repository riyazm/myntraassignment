package com.riyazm.memorygame.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.riyazm.memorygame.R;
import com.riyazm.memorygame.interfaces.ListItemClickInterface;
import com.riyazm.memorygame.models.Photo;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by muhammadriyaz on 07/03/17.
 */

public class PhotosListAdapter extends RecyclerView.Adapter<PhotosListAdapter.PhotoViewHolder> {
    int layoutResourceID;
    List<Photo> items;
    Context context;
    ListItemClickInterface listItemClickInterface;

    public PhotosListAdapter(int layoutResourceID, List<Photo> items, Context context) {
        this.layoutResourceID = layoutResourceID;
        this.items = items;
        this.context = context;
    }

    public void setListItemClickInterface(ListItemClickInterface listItemClickInterface) {
        this.listItemClickInterface = listItemClickInterface;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View mView = LayoutInflater.from(parent.getContext()).inflate(layoutResourceID, parent, false);
        final PhotoViewHolder mViewHolder = new PhotoViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listItemClickInterface != null) {
                    listItemClickInterface.onClick(mViewHolder.getAdapterPosition());
                }

            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {

        Picasso.with(context)
                .load(items.get(position).media.m)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.thumbnail);
        holder.flippedView.setVisibility(items.get(position).reveal ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        LinearLayout flippedView;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            flippedView = (LinearLayout) itemView.findViewById(R.id.flipped_view);
        }
    }
}
