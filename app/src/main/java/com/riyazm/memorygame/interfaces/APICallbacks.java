package com.riyazm.memorygame.interfaces;

import android.util.Log;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by muhammadriyaz on 12/02/17.
 */

public abstract class APICallbacks {
    static final String TAG = APICallbacks.class.getSimpleName();

    public void onPreExecute(String requestURL) {
        // do anything global here. send analytics or print logs
        Log.d(TAG, "hitting url: " + requestURL);
    }

    public void onPostExecuteSuccess(String requestURL, String response) {
        // cut down response object if it is too long
        Log.i(TAG, "success request url: " + requestURL + " response: " + response);
    }

    public void onPostExecuteFail(String requestURL, VolleyError error) {
        Log.e(TAG, "request failed: " + requestURL + " response is: " + error.getMessage());
    }
}