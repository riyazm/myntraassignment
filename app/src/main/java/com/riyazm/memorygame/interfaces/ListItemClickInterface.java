package com.riyazm.memorygame.interfaces;

/**
 * Created by muhammadriyaz on 07/03/17.
 */

public interface ListItemClickInterface {
    void onClick(int position);
}
